import Steps from "./components/Steps";


function App() {
  let steps = [
    {id: 1, title: 'Hello World', content: 'Welcome to learning React!'},
    {id: 2, title: 'Installation', content: 'You can install React from npm.'},
    {id: 3, title: 'Create react app', content: 'Run create-react-app to run project.'},
    {id: 4, title: 'Run init project', content: 'Cd into project and npm start to run project.'},
  ];
  
  return (
    <div >
      <Steps stepsNext = {steps} />
    </div>
  );
}

export default App;
