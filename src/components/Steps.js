import { Component } from "react";

class Steps extends Component {
    render() {
        let { stepsNext } = this.props;
        return (
            <ul>
                {stepsNext.map((element) => { 
                    return <li key={element.id} >
                        Bước {element.id}. {element.title} : {element.content}
                    </li>
                })}
            </ul>
        )
    }
}

export default Steps